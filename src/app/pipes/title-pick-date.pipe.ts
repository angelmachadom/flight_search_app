import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titlePickDate'
})
export class TitlePickDatePipe implements PipeTransform {

  transform(value: any, args: boolean): any {
    if (args === false) {
      return 'DEPARTURE';
    } else {
      return 'ARRIVE';
    }
    }
  }

