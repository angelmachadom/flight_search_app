import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule  } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AirportSelectorComponent } from './components/airport-selector/airport-selector.component';
import { FlightListComponent } from './components/flight-list/flight-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TitlePickDatePipe } from './pipes/title-pick-date.pipe';
import { ArriveDateComponent } from './components/arrive-date/arrive-date.component';
import { DepartureDateComponent } from './components/departure-date/departure-date.component';

@NgModule({
  declarations: [
    AppComponent,
    AirportSelectorComponent,
    FlightListComponent,
    TitlePickDatePipe,
    ArriveDateComponent,
    DepartureDateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
