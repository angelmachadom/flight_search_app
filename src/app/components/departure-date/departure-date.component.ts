import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
    selector: 'app-departure-date',
    templateUrl: './departure-date.component.html',
    styleUrls: ['./departure-date.component.css']
  })


export class DepartureDateComponent implements OnInit {

  @Output() dataDepartureSelection = new EventEmitter();

  dates = new FormGroup({
    departure: new FormControl(),
    arrive: new FormControl()
  });

  constructor() { }
  DATE_DEPARTURE_RESULT: Date;

  ngOnInit() {
  }

  getDepartureDate(date) {
    this.dataDepartureSelection.emit(date);
  }

}
