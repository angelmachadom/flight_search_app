import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArriveDateComponent } from './arrive-date.component';

describe('ArriveDateComponent', () => {
  let component: ArriveDateComponent;
  let fixture: ComponentFixture<ArriveDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArriveDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArriveDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
