import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-arrive-date',
  templateUrl: './arrive-date.component.html',
  styleUrls: ['./arrive-date.component.css']
})



export class ArriveDateComponent implements OnInit {

  @Output() dataArrivalSelection = new EventEmitter();

  dates = new FormGroup({
    departure: new FormControl(),
    arrive: new FormControl()
  });

  constructor() { }
  DATE_ARRIVAL_RESULT: Date;

  ngOnInit() {}

  getArrivalDate(date) {
    this.dataArrivalSelection.emit(date);
  }

}
