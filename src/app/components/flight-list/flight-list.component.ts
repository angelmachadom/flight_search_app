import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.css']
})
export class FlightListComponent implements OnInit {

  DEPARTURE_AIRPORT:string;
  DEPARTURE_DATE: string;
  ITSDATAFETCH: boolean;

  ARRIVAL_AIRPORT:string;
  ARRIVAL_DATE: string;

  RESULT_FLIGHT_SERARCH : {
    flights: string[] 
  }
  

  constructor(private service: AppService) { }

  ngOnInit() {this.ITSDATAFETCH = false}

  getAirportsIataCodes(event){
    this.DEPARTURE_AIRPORT = event.DEPARTURE_IATA_CODE;
    this.ARRIVAL_AIRPORT = event.ARRIVAL_IATA_CODE;
  }

  getDepartureDate2(event){
    this.DEPARTURE_DATE = event;
  }

  getArrivalDate(event){
    this.ARRIVAL_DATE = event;
  }

  findFlight(){
    this.service.getFlights(this.DEPARTURE_AIRPORT, this.ARRIVAL_AIRPORT, this.DEPARTURE_DATE, this.ARRIVAL_DATE)
    .subscribe(data => { this.RESULT_FLIGHT_SERARCH = data.flights; this.ITSDATAFETCH = true})
   
  }
}
