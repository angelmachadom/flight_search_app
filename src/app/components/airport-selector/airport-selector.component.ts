import { Component, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { AppService } from 'src/app/service/app.service';
import { FormControl, FormGroup } from '@angular/forms';
import { PercentPipe } from '@angular/common';
import { JitEvaluator } from '@angular/compiler';


@Component({
  selector: 'app-airport-selector',
  templateUrl: './airport-selector.component.html',
  styleUrls: ['./airport-selector.component.css']
})

export class AirportSelectorComponent implements OnInit {

  @Output() sendingAirportsIataCodes = new EventEmitter<{
    DEPARTURE_IATA_CODE: string;
    ARRIVAL_IATA_CODE: string;
  }>();

  ENTIRE_DATA_REQUEST: any = 0;
  LIST_OF_CITYS: string[] = [];
  ARRIVAL_IATA_CODE: string;
  DEPARTURE_IATA_CODE: string;
 
  INFORMATION_OF_DEPARTURE: {
    iataCode: string,
    name: string,
    base?: boolean,
    latitude?: number,
    longitude?: number
  };
  
  DESTINATIONS_DETAIL: {
    iataCode: string,
    name: string,
    base?: boolean,
    latitude?: number,
    longitude?: number
  };

  airport = new FormGroup({
    city: new FormControl(),
    destination: new FormControl()
  });



  constructor(private service: AppService) {}
  FLIGHT_API_URL = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';

  ngOnInit() {
    this.service.getDataFromApi(this.FLIGHT_API_URL).subscribe(data => {
      this.ENTIRE_DATA_REQUEST = new Object(data);
      this.LIST_OF_CITYS = this.ENTIRE_DATA_REQUEST.airports.map(item => { return {iataCode: item.iataCode, name: item.name}});
      // DOING USING FUNCTIONS
      //this.airport.controls['city'].valueChanges.subscribe(city => this.departureAirport(city));
      //DOIN REFACTOR
      this.airport.controls['city'].valueChanges.subscribe(departureIataCode => this.departureAirport(departureIataCode));
      this.airport.controls['destination'].valueChanges.subscribe(destination => this.sendInformationOfAirports(this.DEPARTURE_IATA_CODE, destination ));
  });

  }


  // departureAirport(city) {
  //    this.INFORMATION_OF_DEPARTURE =  this.ENTIRE_DATA_REQUEST.airports.find(item => item.name === city);
  //    console.log(this.INFORMATION_OF_DEPARTURE);
  //    this.DESTINATIONS = this.ENTIRE_DATA_REQUEST.routes[this.INFORMATION_OF_DEPARTURE.iataCode];

  //    this.DESTINATIONS_DETAIL = this.DESTINATIONS.map(iatacode => this.ENTIRE_DATA_REQUEST.airports.find(item =>
  //       item.iataCode === iatacode));
  // }

  departureAirport(departureIataCode) {
    this.DEPARTURE_IATA_CODE = departureIataCode;
    this.DESTINATIONS_DETAIL = this.ENTIRE_DATA_REQUEST.routes[departureIataCode].map(iatacode => this.ENTIRE_DATA_REQUEST.airports.find(item =>
      item.iataCode === iatacode));

 }

 sendInformationOfAirports(DEPARTURE_IATA_CODE, ARRIVAL_IATA_CODE){
  this.sendingAirportsIataCodes.emit({
    DEPARTURE_IATA_CODE: DEPARTURE_IATA_CODE,
    ARRIVAL_IATA_CODE: ARRIVAL_IATA_CODE
   
  })
 }

  
}
