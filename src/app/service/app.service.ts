import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private service: HttpClient) { }

  getDataFromApi(url): Observable<any> {
    return this.service.get(url);
  }

  getFlights(departureAirport: string, arrivalAirport: string, departureDate: string, arrivalDate: string ) {
    const URL = `https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/from/${departureAirport}/to/${arrivalAirport}
    /${departureDate}/${arrivalDate}/250/unique/?limit=15&offset-0`;
    return this.service.get(URL);
  }

}
